/*****************************
 * @file	MainWindow.h
 * @date	Aug 20, 2017
 * @author	Hans Hirschberg
 * @brief	brief description
 * 
 * full description
 */
#ifndef WINDOWS_MAINWINDOW_H_
#define WINDOWS_MAINWINDOW_H_

#include <gtkmm.h>

#include "../widgets/ProductListWidget.h"

/*****************************
 * @brief	brief desc
 *	
 * long desc
 */
class MainWindow : public Gtk::Window{
public:
	MainWindow();
	virtual ~MainWindow();
private:
	void setupElements();
	void setupWindow();

	RequestListWidget _produktList;

	Gtk::Button _testButton;
};

#endif /* WINDOWS_MAINWINDOW_H_ */
