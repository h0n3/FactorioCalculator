/*****************************
 * @file	MainWindow.cpp
 * @date	Aug 20, 2017
 * @author	Hans Hirschberg
 * @brief	brief description
 * 
 * full description
 */
#include "MainWindow.h"

MainWindow::MainWindow()
:_produktList{}{
	setupWindow();
}

MainWindow::~MainWindow() {
	// TODO Auto-generated destructor stub
}

void MainWindow::setupElements(){

	add(_produktList);

	_produktList.display(FactorioModel::instance()._produktList,FactorioModel::instance()._produktListLength);


	_testButton = Gtk::Button("Test");
	add(_testButton);
}

void MainWindow::setupWindow() {
	set_title("Factorio Planner");
	set_default_size(800,600);

	setupElements();

	show_all_children();
}
