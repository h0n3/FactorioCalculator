/*****************************
 * @file	FactorioModel.cpp
 * @date	Aug 20, 2017
 * @author	Hans Hirschberg
 * @brief	brief description
 * 
 * full description
 */
#include "FactorioModel.h"

// A= 0, B=1, C=2, D=3, E=4, F=5

const Product FactorioModel::_produktList[] = {
		{
			0,
			"A",
			0.5,
			{
				{1, 1}, // 1B
				{2, 2}  // 2C
			},
			{

			}
		},
		{
			1,
			"B",
			0.5,
			{
				{3, 2}, // 2D
				{4, 2}  // 2E
			},
			{
				0 		// A
			}
		},
		{
			2,
			"C",
			0.5,
			{
				{3, 1}, // 1D
				{5, 2}  // 2F
			},
			{
				0 		// A
			}
		},
		{
			3,
			"D",
			0.5,
			{

			},
			{
				1, 		// B
				2		// C
			}
		},
		{
			4,
			"E",
			0.5,
			{

			},
			{
				1 		// A
			}
		},
		{
			5,
			"F",
			0.5,
			{

			},
			{
				2		// C
			}
		},
};

const idT FactorioModel::_produktListLength = sizeof(_produktList) / sizeof(_produktList[0]);
idT FactorioModel::_at = 0;

std::vector<Request> FactorioModel::calculateProductsNeeded(
		std::vector<Request>& request) {

	std::cout << "FactorioModel::calculateProductsNeeded was called" << std::endl;

	initRequested(request);

	std::cout << "Init completed starting to calculate Dependencies" << std::endl;

	while (!allRequested()){

		idT activeProductID = getNextProductWithMetDependencies();

		std::cout << "\t Got Product with met dependencies : id=" << activeProductID  << "  desc=" << getProduct(activeProductID).getName();
		std::cout.flush();

		handleProduct(activeProductID);

		std::cout << "....handled it";
		std::cout.flush();

		updateRequests(activeProductID);

		std::cout << "....updated requests" << std::endl;
	}

	std::cout << "All products dependencies calculated generating output....." << std::endl;

	return getListofAllRequests();

}



Product::Product(idT id, char* name, double productionTime,
		std::initializer_list<Relation> ingredients,
		std::initializer_list<idT> depending):
		_id{id}, _name{name}, _productionTime{productionTime}, _ingredients{ingredients}, _depending{depending}
		{}

void FactorioModel::resetRequested() {
	_requested = std::vector<RequestData>();
	for (idT productID = 0; productID < _produktListLength; ++productID){
		const Product&  actProduct = getProduct(productID);
		RequestData actRequest;

		actRequest.productId = productID;
		actRequest.handled = false;
		actRequest.sumRequested = 0;
		actRequest.requests = std::vector<Request>();
		for (auto& dependencie : actProduct.getDepending()){
			actRequest.requests.push_back(Request{dependencie, 0});
		}

		_requested.push_back(std::move(actRequest));
	}

}

const Product& FactorioModel::getProduct(idT productID){
	for (auto& product : _produktList){
		if (productID == product.getId()) return product;
	}
	throw(std::string("Exception: Product with id:") + std::to_string(productID) + " not found!"); // TODO add type
}

FactorioModel::RequestData& FactorioModel::getRequestedProduct(idT productID){
	for (auto& request : _requested){
		if (productID == request.productId) return request;
	}
	throw(std::string("Exception: RequestData for product with id:") + std::to_string(productID) + " not found!"); // TODO add type
}

void FactorioModel::initRequested(std::vector<Request>& request){
	resetRequested();

	for (auto& req : request){
		getRequestedProduct(req.requestedID).requests.push_back(Request{_produktListLength, req.requestedAmount});
	}
}

void FactorioModel::handleProduct(idT productID){
	auto& activeRequest = getRequestedProduct(productID);

	idT sum = 0;

	for (auto& req :activeRequest.requests){
		sum += req.requestedAmount;
	}
	activeRequest.sumRequested = sum;
	activeRequest.handled = true;

}

bool FactorioModel::allRequested() {
	for (auto& entry : _requested){
		if (entry.handled == false) return false;
	}
	return true;
}

idT FactorioModel::getNextProductWithMetDependencies(){
	for (auto product : _produktList){

		bool metDependencies = hasProductMetDependencies(product.getId());
		bool notHandled = !getRequestedProduct(product.getId()).handled;

		if ((metDependencies) && (notHandled) ){
			return product.getId();
		}
	}

	throw("Could not another unhandled product");
}



bool FactorioModel::hasProductMetDependencies(idT productID) {
	auto& activeProduct = getProduct(productID);

	bool ret = true;

	for (auto dep : activeProduct.getDepending()){
		if (!(wasProductHandled(dep))) ret = false;
	}
	return ret;
}

bool FactorioModel::wasProductHandled(idT productID) {
	if (productID < _produktListLength) return getRequestedProduct(productID).handled; else return true;
}

void FactorioModel::updateRequests(idT productID){
	auto requestedProduct = getRequestedProduct(productID);

	idT amountRequested = requestedProduct.sumRequested;

	auto& activeProduct = getProduct(productID);;
	for (auto& ingridient : activeProduct.getIngredients()){
		idT requestID  = ingridient.ID;
		for (auto& requested : getRequestedProduct(requestID).requests){
			if (requested.requestedID == productID){
				requested.requestedAmount = amountRequested * ingridient.amnt;
			}
		}
	}
}


FactorioModel::FactorioModel() {
	// TODO Auto-generated constructor stub

}

FactorioModel::~FactorioModel() {
	// TODO Auto-generated destructor stub
}

std::vector<char*> FactorioModel::getProductNames() {
}

std::vector<Request> FactorioModel::getListofAllRequests(){
	std::vector<Request> ret;

	std::cout << "trying to generate output... calculated size of _requested ar is:" << sizeof(_requested)/ sizeof(_requested[0]) << std::endl;

	for (idT product = 0; product < _produktListLength; ++product){

		std::cout << "scanning product id: " << product << std::endl;

		auto& req = _requested[product];
		if (req.sumRequested){
			ret.push_back(Request{product, req.sumRequested});
		}
	}
	return ret;
}

idT Product::getId() const {
	return _id;
}

char* Product::getName() const {
	return _name;
}

double Product::getProductionTime() const {
	return _productionTime;
}

const std::vector<Product::Relation>& Product::getIngredients() const {
	return _ingredients;
}

const std::vector<idT>& Product::getDepending() const {
	return _depending;
}
