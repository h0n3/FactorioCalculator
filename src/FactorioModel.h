/*****************************
 * @file	FactorioModel.h
 * @date	Aug 20, 2017
 * @author	Hans Hirschberg
 * @brief	brief description
 * 
 * full description
 */
#ifndef FACTORIOMODEL_H_
#define FACTORIOMODEL_H_

#include <iostream>

#include <vector>
#include <gtkmm.h>

/*****************************
 * @brief	brief desc
 *	
 * long desc
 */
using idT = std::size_t;

class Product{
public:
	struct Relation{
		Relation(idT id, idT amount):ID{id},amnt{amount}{}

		idT ID;
		idT amnt;
	};

	Product();
	Product(idT id,
			char* name,
			double productionTime,
			std::initializer_list<Relation> ingredients,
			std::initializer_list<idT> depending);

	idT getId() const;
	char* getName() const;
	double getProductionTime() const;
	const std::vector<Relation>& getIngredients() const;
	const std::vector<idT>& getDepending() const;

private:
	idT _id;

	char*  _name;

	double _productionTime;

	const std::vector<Relation> _ingredients;
	const std::vector<idT> _depending;

};

struct Request{
	Request(idT id, idT amount):requestedID{id}, requestedAmount{amount}{}

	idT requestedID;
	idT requestedAmount;
};



class FactorioModel {
private:
	struct RequestData{
		idT productId;

		bool handled;

		idT sumRequested;
		std::vector<Request> requests;
	};
public:
	static FactorioModel& instance(){
		static FactorioModel instance;
		return instance;
	}

	virtual ~FactorioModel();

	std::vector<Request> calculateProductsNeeded(std::vector<Request>& request);

	void handleProduct(idT product);

	const Product& getProduct(idT productID);
	RequestData& getRequestedProduct(idT productID);

	std::vector<char*> getProductNames();

	static const Product _produktList[];
	static const idT _produktListLength;

private:
	FactorioModel();

	static idT _at;

	std::vector<RequestData> _requested;

	void initRequested(std::vector<Request>& request);
	void resetRequested();
	bool allRequested();

	idT getNextProductWithMetDependencies();

	idT getNextProduct();
	bool isProductListEmpty();
	void resetList();

	bool hasProductMetDependencies(idT productID);
	bool wasProductHandled(idT productID);

	void updateRequests(idT productID);

	std::vector<Request> getListofAllRequests();



};

#endif /* FACTORIOMODEL_H_ */
