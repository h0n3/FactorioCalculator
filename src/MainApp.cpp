/*****************************
 * @file	MainApp.cpp
 * @date	Aug 20, 2017
 * @author	Hans Hirschberg
 * @brief	brief description
 * 
 * full description
 */
#include "MainApp.h"

MainApp::MainApp() {
	Glib::set_application_name("HNN Gui");

}

MainApp::~MainApp() {
	// TODO Auto-generated destructor stub
}

void MainApp::on_startup() {
	Gtk::Application::on_startup();
}

void MainApp::on_activate() {
	Gtk::Application::on_activate();

	displayMainWindow();

}

Glib::RefPtr<MainApp> MainApp::create(){
	return Glib::RefPtr<MainApp>(new MainApp());
}


int main(){
	Glib::RefPtr<MainApp> app = MainApp::create();

	app->run();

	try{

	std::vector<Request> requestCommand{{0,2},{3,4}};

	auto inst  = FactorioModel::instance();

	auto ret = inst.calculateProductsNeeded(requestCommand);

	for (auto& retReq : ret){
		std::cout << "Calculated Amount of " << inst.getProduct(retReq.requestedID).getName() << " is " << retReq.requestedAmount << std::endl;
	}

	} catch(std::string& str){
		std::cout << str << std::endl;
	}
}

void MainApp::displayMainWindow() {
	_mainWindow = new MainWindow();
	add_window(*_mainWindow);
	_mainWindow->show_all();
}
