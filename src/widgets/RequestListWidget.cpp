/*****************************
 * @file	RequestListWidget.cpp
 * @date	Aug 21, 2017
 * @author	Hans Hirschberg
 * @brief	brief description
 * 
 * full description
 */
#include "RequestListWidget.h"

RequestListWidget::RequestListWidget()
: Gtk::Box{Gtk::ORIENTATION_VERTICAL}, _bbControls{Gtk::ORIENTATION_HORIZONTAL}, _data{} {
	setupPage();

}

RequestListWidget::~RequestListWidget() {
	// TODO Auto-generated destructor stub
}

void RequestListWidget::setData(std::vector<Request>& requests){
	_data = requests;
	display();
}

void RequestListWidget::setupPage() {
	setupList();
	pack_start(_scrListContainer);

	setupButtons();
	pack_start(_bbControls);

	show_all();
}

void RequestListWidget::setupButtons(){
	_btnAdd = Gtk::Button("Add");
	_btnAdd.signal_clicked().connect(sigc::mem_fun(*this,&RequestListWidget::onBtnAddClicked));

	_btnEdt = Gtk::Button("Edit");
	_btnEdt.signal_clicked().connect(sigc::mem_fun(*this,&RequestListWidget::onBtnEdtClicked));

	_btnDel = Gtk::Button("Delete");
	_btnDel.signal_clicked().connect(sigc::mem_fun(*this,&RequestListWidget::onBtnDelClicked));

	_bbControls.add(_btnAdd);
	_bbControls.add(_btnEdt);
	_bbControls.add(_btnDel);
}

void RequestListWidget::setupList(){
	_trRequestListStore  = Gtk::TreeStore::create(_trRequestListModel);

	_trRequestList.set_model(_trRequestListStore);
	_scrListContainer.add(_trRequestList);
}

void RequestListWidget::appendRequestToList(const Request& request) {
	Gtk::TreeStore::Row row = *(_trRequestListStore->append());

	row[_trRequestListModel._colAmount] = request.requestedAmount;
	row[_trRequestListModel._colDescription] = FactorioModel::instance().getProduct(request.requestedID).getName();
}

void RequestListWidget::display(){
	_trRequestListStore->clear();

	for (auto& request : _data){
		appendRequestToList(request);
	}
}

void RequestListWidget::onBtnAddClicked(){

}

void RequestListWidget::onBtnDelClicked() {
}



void RequestListWidget::onBtnEdtClicked() {
}

RequestDialog::RequestDialog(Gtk::Window& parent)
:Gtk::Dialog{"Add Request", parent}, _data{}, _boxLayout{Gtk::ORIENTATION_VERTICAL}{
	setupLayout();
}

RequestDialog::RequestDialog(Gtk::Window& parent, Request& request) {
}

RequestDialog::~RequestDialog() {
}

Request& RequestDialog::getPayload() {
}

void RequestDialog::setupProductSelector() {
}

void RequestDialog::setupAmountSpinner() {
}

void RequestDialog::setupButtons() {
	_btnAccept = Gtk::Button("Accept");
	_btnCancel = Gtk::Button("Cancel");

	_bbControls.add(_btnAccept);
	_bbControls.add(_btnCancel);
}


void RequestDialog::setupLayout() {

	setupProductSelector();
	_boxLayout.add(_cbProductSelector);

	setupAmountSpinner();
	_boxLayout.add(_spAmount);

	setupButtons();
	_boxLayout.add(_bbControls);

}

