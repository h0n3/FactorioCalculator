/*****************************
 * @file	ProductListWidget.cpp
 * @date	Aug 20, 2017
 * @author	Hans Hirschberg
 * @brief	brief description
 * 
 * full description
 */
#include "ProductListWidget.h"

ProductListWidget::ProductListWidget() {
	setupTreeView();
	add(_trProductList);
	show_all();
}

ProductListWidget::~ProductListWidget() {
	// TODO Auto-generated destructor stub
}

void ProductListWidget::fillProductListModelRow(idT id,
		Glib::ustring description, idT childId, Glib::ustring childDescription,
		Gtk::TreeModel::iterator iter) {
	Gtk::TreeModel::Row row = *(iter);

	row[_trProductListModel._colID] = id;
	row[_trProductListModel._colDescription] = description;
	row[_trProductListModel._colChildID] = childId;
	row[_trProductListModel._colChildDescription] = childDescription;
}


void ProductListWidget::appendProductDescSection(const Gtk::TreeModel::iterator& iter,
		const Product& product) {

	auto modelInstance = FactorioModel::instance();

	auto id = product.getId();
	Glib::ustring description = modelInstance.getProduct(id).getName();
	fillProductListModelRow(id, description, 0, "", iter);
}

void ProductListWidget::appendIngredientsSection(const Gtk::TreeModel::iterator& iter,
		const std::vector<Product::Relation>& ingredients) {
	auto ingredientsSection = _trProductListStore->append(iter->children());
	fillProductListModelRow(0,"",ingredients.size(),"Ingredients",ingredientsSection);

	auto modelInstance = FactorioModel::instance();
	idT id = 0;
	for (auto& ingredient : ingredients){
		auto ingredientSection = _trProductListStore->append(ingredientsSection->children());
		fillProductListModelRow(0,"",ingredient.amnt,modelInstance.getProduct(ingredient.ID).getName(),ingredientSection);
	}
}

void ProductListWidget::appendDependencieSection(const Gtk::TreeModel::iterator& iter,
		const std::vector<idT>& dependencies) {
	auto dependenciesSection = _trProductListStore->append(iter->children());
	fillProductListModelRow(0,"",dependencies.size(),"dependencies",dependenciesSection);

	auto modelInstance = FactorioModel::instance();
	idT id = 0;
	for (auto& dependencie : dependencies){
		auto dependencieSection = _trProductListStore->append(dependenciesSection->children());

		auto ReferencedIngridient = modelInstance.getProduct(id);

		fillProductListModelRow(0,"",ReferencedIngridient.getId(),ReferencedIngridient.getName(),dependencieSection);
		++id;
	}
}

void ProductListWidget::appendProductRow(const Product& product) {
	auto productRow = _trProductListStore->append();
	appendProductDescSection(productRow, product);
	appendIngredientsSection(productRow, product.getIngredients());
	appendDependencieSection(productRow, product.getDepending());

}

void ProductListWidget::display(const Product* products,  std::size_t  numProducts){
	auto instance = FactorioModel::instance();

	for (std::size_t productID = 0; productID < numProducts; ++productID){
		appendProductRow(instance.getProduct(productID));
	}
}

void ProductListWidget::setupTreeView() {
	_trProductListStore = Gtk::TreeStore::create(_trProductListModel);
	_trProductList = Gtk::TreeView(_trProductListStore);

	_trProductListColID.set_title("Product ID");
	_trProductListColID.pack_start(_trProductListColIDRenderer);
	_trProductListColID.set_cell_data_func(_trProductListColIDRenderer,
											sigc::mem_fun(*this,
															&ProductListWidget::drawID
														  )
										  );
	_trProductList.append_column(_trProductListColID);

	_trProductListColDesc.set_title("Product Desc");
	_trProductListColDesc.pack_start(_trProductListColDescRenderer);
	_trProductListColDesc.set_cell_data_func(_trProductListColDescRenderer,
											sigc::mem_fun(*this,
															&ProductListWidget::drawDesc
														  )
										  );
	_trProductList.append_column(_trProductListColDesc);

	_trProductListColChildDesc.set_title(" child desc");
	_trProductListColChildDesc.pack_start(_trProductListColChildDescRenderer);
	_trProductListColChildDesc.set_cell_data_func(_trProductListColChildDescRenderer,
											sigc::mem_fun(*this,
															&ProductListWidget::drawChildDesc
														  )
										  );
	_trProductList.append_column(_trProductListColChildDesc);


	_trProductListColChildAmount.set_title("amount");
	_trProductListColChildAmount.pack_start(_trProductListColChildAmountRenderer);
	_trProductListColChildAmount.set_cell_data_func(_trProductListColChildAmountRenderer,
											sigc::mem_fun(*this,
															&ProductListWidget::drawChildAmount
														  )
										  );
	_trProductList.append_column(_trProductListColChildAmount);

}

void ProductListWidget::drawID(Gtk::CellRenderer*,
		const Gtk::TreeModel::iterator& iter) {
	if (iter){
		Gtk::TreeStore::Row row = *iter;
		idT depth = _trProductListStore->iter_depth(row);

		Glib::ustring caption = "";

		if (depth == 0) caption = std::to_string(row[_trProductListModel._colID]);

		_trProductListColIDRenderer.property_text() = caption;
	}
}

void ProductListWidget::drawDesc(Gtk::CellRenderer*,
		const Gtk::TreeModel::iterator& iter) {
	if (iter){
		Gtk::TreeStore::Row row = *iter;
		idT depth = _trProductListStore->iter_depth(row);

		Glib::ustring caption = "";

		if (depth == 0) caption = row[_trProductListModel._colDescription];
		else if (depth == 1) caption = row[_trProductListModel._colChildDescription];

		_trProductListColDescRenderer.property_text() = caption;
	}
}

void ProductListWidget::drawChildDesc(Gtk::CellRenderer*,
		const Gtk::TreeModel::iterator& iter) {
	if (iter){

		Gtk::TreeStore::Row row = *iter;
		idT depth = _trProductListStore->iter_depth(row);

		Glib::ustring caption = "";

		if (depth > 1) caption = row[_trProductListModel._colChildDescription];



		_trProductListColChildDescRenderer.property_text() = caption;
	}
}

void ProductListWidget::drawChildAmount(Gtk::CellRenderer*,
		const Gtk::TreeModel::iterator& iter) {
	if (iter){
		Gtk::TreeStore::Row row = *iter;
		idT depth = _trProductListStore->iter_depth(row);

		Glib::ustring caption = "";

		if (depth > 0) caption = std::to_string(row[_trProductListModel._colChildID]);


		_trProductListColChildAmountRenderer.property_text() = caption;
	}
}
