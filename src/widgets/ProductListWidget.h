/*****************************
 * @file	ProductListWidget.h
 * @date	Aug 20, 2017
 * @author	Hans Hirschberg
 * @brief	brief description
 * 
 * full description
 */
#ifndef WIDGETS_PRODUCTLISTWIDGET_H_
#define WIDGETS_PRODUCTLISTWIDGET_H_

#include <gtkmm.h>

#include "../FactorioModel.h"

class ProductListModel : public Gtk::TreeModel::ColumnRecord{
public:
	ProductListModel(){
		add(_colID);
		add(_colDescription);

		add(_colChildID);
		add(_colChildDescription);
	}

	// LVL 0 ProductData
	Gtk::TreeModelColumn<idT> _colID;
	Gtk::TreeModelColumn<Glib::ustring> _colDescription;

 	//LVL 1 Ingridients + Dependencies
	Gtk::TreeModelColumn<idT> _colChildID; // TODO theese names suck hella bad son
	Gtk::TreeModelColumn<Glib::ustring> _colChildDescription;
};




/*****************************
 * @brief	brief desc
 *	
 * long desc
 */
class ProductListWidget: public Gtk::ScrolledWindow {
public:
	ProductListWidget();
	virtual ~ProductListWidget();

	void display(const Product* products,  std::size_t  numProducts);
protected:

	void fillProductListModelRow(idT id, Glib::ustring description, idT childId, Glib::ustring childDescription, Gtk::TreeModel::iterator iter);

	void appendProductRow(const Product& product);

	void appendProductDescSection(const Gtk::TreeModel::iterator& iter, const Product& product);
	void appendIngredientsSection(const Gtk::TreeModel::iterator& iter, const std::vector<Product::Relation>& ingredients);
	void appendDependencieSection(const Gtk::TreeModel::iterator& iter, const std::vector<idT>& dependencies);

	void setupTreeView();

	void drawID(Gtk::CellRenderer*, const Gtk::TreeModel::iterator& iter);
	void drawDesc(Gtk::CellRenderer*, const Gtk::TreeModel::iterator& iter);

	void drawChildDesc(Gtk::CellRenderer*, const Gtk::TreeModel::iterator& iter);
	void drawChildAmount(Gtk::CellRenderer*, const Gtk::TreeModel::iterator& iter);

	ProductListModel _trProductListModel;

	Gtk::TreeView _trProductList;
	Glib::RefPtr<Gtk::TreeStore> _trProductListStore;

	Gtk::TreeView::Column _trProductListColID;
	Gtk::TreeView::Column _trProductListColDesc;
	Gtk::TreeView::Column _trProductListColChildDesc;
	Gtk::TreeView::Column _trProductListColChildAmount;

	Gtk::CellRendererText _trProductListColIDRenderer;
	Gtk::CellRendererText _trProductListColDescRenderer;
	Gtk::CellRendererText _trProductListColChildDescRenderer;
	Gtk::CellRendererText _trProductListColChildAmountRenderer;
};

#endif /* WIDGETS_PRODUCTLISTWIDGET_H_ */
