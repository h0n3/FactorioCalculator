/*****************************
 * @file	RequestListWidget.h
 * @date	Aug 21, 2017
 * @author	Hans Hirschberg
 * @brief	brief description
 * 
 * full description
 */
#ifndef WIDGETS_REQUESTLISTWIDGET_H_
#define WIDGETS_REQUESTLISTWIDGET_H_

#include <gtkmm.h>



class RequestListModel : public Gtk::TreeModel::ColumnRecord{
public:
	RequestListModel(){
		add(_colDescription);
		add(_colAmount);
	}

	// LVL 0 ProductData
	Gtk::TreeModelColumn<Glib::ustring> _colDescription;
	Gtk::TreeModelColumn<idT> _colAmount;



};

class RequestDialog : public Gtk::Dialog{
public:
	RequestDialog(Gtk::Window& parent);
	RequestDialog(Gtk::Window& parent, Request& request);

	~RequestDialog();

	Request& getPayload();

private:
	void setupLayout();

	void setupProductSelector();
	void setupAmountSpinner();

	void setupButtons();

	Request _data;

	Gtk::Box _boxLayout;

	Gtk::ComboBox _cbProductSelector;
	Gtk::Spinner  _spAmount;

	Gtk::ButtonBox _bbControls;

	Gtk::Button _btnAccept;
	Gtk::Button _btnCancel;
};


/*****************************
 * @brief	brief desc
 *	
 * long desc
 */
class RequestListWidget: public Gtk::Box {
public:
	RequestListWidget();
	virtual ~RequestListWidget();

	void setData(std::vector<Request>& requests);

private:

	std::vector<Request> _data;

	Gtk::ButtonBox _bbControls;
	Gtk::Button _btnAdd;
	Gtk::Button _btnDel;
	Gtk::Button _btnEdt;


	Gtk::ScrolledWindow _scrListContainer;

	RequestListModel _trRequestListModel;

	Gtk::TreeView _trRequestList;
	Glib::RefPtr<Gtk::TreeStore> _trRequestListStore;


	void setupPage();

	void setupButtons();

	void setupList();

	void display();

	void appendRequestToList(const Request& request);

	void onBtnAddClicked();
	void onBtnDelClicked();
	void onBtnEdtClicked();


};

#endif /* WIDGETS_REQUESTLISTWIDGET_H_ */
