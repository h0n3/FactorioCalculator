/*****************************
 * @file	MainApp.h
 * @date	Aug 20, 2017
 * @author	Hans Hirschberg
 * @brief	brief description
 * 
 * full description
 */
#ifndef MAINAPP_H_
#define MAINAPP_H_

#include <gtkmm.h>
#include <iostream>

#include "FactorioModel.h"

#include "windows/MainWindow.h"

/*****************************
 * @brief	brief desc
 *	
 * long desc
 */
class MainApp : public Gtk::Application {
protected:
	MainApp();
	virtual ~MainApp();

	void on_startup() override;
	void on_activate() override;

public:
	static Glib::RefPtr<MainApp> create();

private:
	void displayMainWindow();

	MainWindow* _mainWindow;

};

#endif /* MAINAPP_H_ */
